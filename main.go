package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
)

func main() {
	input_reader := bufio.NewReader(os.Stdin)
	input_lines := bufio.NewScanner(input_reader)
	for input_lines.Scan() {
		line := input_lines.Text()
		var number float64
		fmt.Sscanf(line, "%f\n", &number)
		bits := math.Float64bits(number)
		sign := (bits >> 63) & 1
		biased_exponent := (bits >> 52) & 0x3ff
		mantissa := bits & ((1 << 52) - 1)
		fmt.Printf("%01b %011b %052b\n",
			sign, biased_exponent, mantissa)
	}
}
